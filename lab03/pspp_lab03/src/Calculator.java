
import com.sun.org.apache.bcel.internal.*;
import com.sun.org.apache.bcel.internal.generic.*;

public class Calculator implements ICodeGenStart
{

	@Override
	public void start() throws Exception {
		// TODO Auto-generated method stub
		
		ArithmeticParser.expr();
		CodeGen.il.append(new DRETURN());
	}
	
	public static void main(String[] args) throws Exception
	{
		Scanner.init("37+(12-2)/2");
		CodeGen.init();
		Scanner.scan();
		ICalculator calculator = (ICalculator)CodeGen.buildClass(ICalculator.class, new Calculator());
		
		System.out.println("Result: " + calculator.calc(0));
				
	}
	
}
