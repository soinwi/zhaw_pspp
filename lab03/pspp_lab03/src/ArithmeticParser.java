import java.util.Stack;

import javax.swing.plaf.basic.DefaultMenuLayout;

import com.sun.org.apache.bcel.internal.*;
import com.sun.org.apache.bcel.internal.generic.*;

/**
 * User: Karl Rege
 */

//Wiederverwendet von lab02

class ArithmeticParser {
	
//	private static Stack<Double> _stack = new Stack<Double>();

	static void expr()	throws Exception {
		term();
		while (Scanner.la == Token.PLUS 
				|| Scanner.la == Token.MINUS) {
			Scanner.scan();
			int op = Scanner.token.kind;
			term();
			if(op == Token.PLUS)
			{
				CodeGen.il.append(new DADD());
			}
			else
			{
				CodeGen.il.append(new DSUB());
			}
//			double second = _stack.pop();
//			double first = _stack.pop();
//			double result = 0;
//			 result = first + second;}else{result = first - second;}
//			_stack.push(result);
		}
	}

	static void term()	throws Exception {
		factor();
		while (Scanner.la == Token.TIMES || Scanner.la == Token.SLASH) {
			Scanner.scan();
			int op = Scanner.token.kind;
			factor();
//			double second = _stack.pop();
//			double first = _stack.pop();
//			double result = 0;
//			if(op == Token.TIMES){result = first * second;}else{result = first / second;}
//			_stack.push(result);
			if(op == Token.TIMES)
			{
				CodeGen.il.append(new DMUL());
			}
			else
			{
				CodeGen.il.append(new DDIV());
			}
			
		}
	}

	static void factor() throws Exception {
		if (Scanner.la == Token.LBRACK) {
			Scanner.scan();
			expr();
			Scanner.check(Token.RBRACK);
		} else if (Scanner.la == Token.NUMBER) {
			Scanner.scan();
			int c = CodeGen.cp.addDouble(Scanner.token.val);
			CodeGen.il.append(new LDC2_W(c));
//			_stack.push(Scanner.token.val);
		}
		else if(Scanner.la == Token.IDENT){
			Scanner.scan();
//			_stack.push(Scanner.token.val);
			int c = CodeGen.cp.addDouble(Scanner.token.val);
			CodeGen.il.append(new LDC2_W(c));
		}
	}


   public static void main(String[] args) throws Exception {
   	Scanner.init("4*PI+E");
   	Scanner.scan();
   	expr();
   	//System.out.println("result="+_stack.pop());
   }
      
}
