public interface ICalculator {
    double calc(double arg0);
}
