import java.io.IOException;
import java.lang.reflect.Method;

import com.sun.org.apache.bcel.internal.*;
import com.sun.org.apache.bcel.internal.generic.*;


public class ILSample implements ICodeGenStart {

	public void start() {
		// Instruction Example: 5 + 8
		// load int const 5 on stack
		int c1 = CodeGen.cp.addInteger(5);
		CodeGen.il.append(new LDC_W(c1));
		
		// load int const 8 on stack
		int c2 = CodeGen.cp.addInteger(8);
		CodeGen.il.append(new LDC_W(c2));
	
		CodeGen.il.append(new IADD());
		CodeGen.il.append(new IRETURN());
	}
  
	public static void main(String[] args) throws Exception {
		CodeGen.init();
		// Erstellt Instanz der Klasse
		ISample calculator = (ISample) CodeGen.buildClass(ISample.class,
				new ILSample());

		// Die generierte Methode wird aufgerufen.
		System.out.println("Calc="+calculator.calc());
	}

}
