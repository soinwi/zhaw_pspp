import java.io.IOException;
import java.lang.reflect.Method;

import com.sun.org.apache.bcel.internal.*;
import com.sun.org.apache.bcel.internal.generic.*;


public class ILSample2 implements ICodeGenStart {

  public void start() {
  	// 1 + arg0 + arg1
    // add double const 1 to stack
    int c1 = CodeGen.cp.addDouble(1.0);
    CodeGen.il.append(new LDC2_W(c1));   
    
    CodeGen.il.append(new DLOAD(CodeGen.local("arg0")));
    CodeGen.il.append(new DADD()); 

    CodeGen.il.append(new DLOAD(CodeGen.local("arg1")));
    CodeGen.il.append(new DADD()); 
     
	CodeGen.il.append(new DRETURN());
  }
  
  public static void main(String[] args) throws Exception {
  	// initialisiere Code Generator
  	CodeGen.init();
  	
    //Erstellt Instanz der Klasse
    ISample2 calculator = (ISample2)CodeGen.buildClass(ISample2.class, new ILSample2());
    //Die generierte Methode wird aufgerufen.
    System.out.println("\nAnswer to the ultimate question of life, the universe, and everything = "+calculator.calc(31,10));
  }

}
