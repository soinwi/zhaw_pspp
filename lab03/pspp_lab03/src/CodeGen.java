import java.io.IOException;
import java.lang.reflect.Method;
import java.lang.reflect.TypeVariable;
import java.util.ArrayList;
import java.util.List;
import java.util.Hashtable;
import java.util.*;

import com.sun.org.apache.bcel.internal.*;
import com.sun.org.apache.bcel.internal.generic.*;
import java.lang.System;


class MyClassLoader extends ClassLoader {
	byte[] code;
    
	MyClassLoader(byte[] code) {
		super(MyClassLoader.class.getClassLoader()); 
		this.code = code;
	}
     
	@Override	
	public Class findClass(String name) {   
		Class c = defineClass(name, code, 0, code.length);
		return c;
	}	
}


public class CodeGen {
	public static ConstantPoolGen cp;
	public static InstructionList il;
	public static MethodGen mg;
	public static Stack<BranchInstruction> branchStack = new Stack<BranchInstruction>();
	public static List<String> locals; // liste der lokalen Variablen
    public static List<String> types = new LinkedList<String>(); // liste der parameter typen
  
	/* bestimme Slot der lokalen Variablen */
	public static int local(String name) {
		int i = locals.indexOf(name);
		if (i < 0) {
			locals.add(name);
			i = locals.indexOf(name);
		}
		i *= 2;
		i++;
		return i;
	}
  
	public static void init() {
		locals = new ArrayList<String>(); 
	}
  
	// Gibt Typen der Parameterliste zur�ck.
	private static Type getParameterType(String parameterTypeName) {
		String[] typeNames = Constants.TYPE_NAMES;
		byte typeByteCode = 0;
		for (int j = 0; j < typeNames.length; j++) {
			if (typeNames[j].equals(parameterTypeName)) {
				typeByteCode = (byte) j;
			}
		}
		Type parameterType = (Type) BasicType.getType(typeByteCode);
		return parameterType;
	}
  
	/**
	 * Erzeugt eine Klasse die das Interface iclass implementiert
	 */ 
	public static Object buildClass(Class iclass, ICodeGenStart codegen) throws Exception {
    
		// Klasse erstellen
		String[] interfaces = new String[] { iclass.getName()};
		ClassGen cg = new ClassGen("MyDynamicClass", // class name
				"java.lang.Object", // super class name
				"<generated>", // file name
				Constants.ACC_PUBLIC, // access flags
				interfaces); // interfaces
    
		// constant pool initialisieren
		cp = cg.getConstantPool();
    
		// instruction list initialisieren
		il = new InstructionList();

		// Informationen f�r die zu generierende Methode
		Method[] myCalcMethodInfos = iclass.getMethods();
		Method myCalcMethodInfo = myCalcMethodInfos[0];
		Class[] parameter = myCalcMethodInfo.getParameterTypes();
    
		for (int i = 0; i < parameter.length; i++) {
			local("arg" + i);
		}
		local("value");  
         
		// Parametertypen der Parameterliste
		Type[] parameterType = new Type[parameter.length];
		// Parameternamen der Parameterliste
		String[] parameterName = new String[parameter.length];
		// Return Type
		Type returnType = getParameterType(
				myCalcMethodInfo.getReturnType().getName());

		// Abf�llen der Parameternamen & -typen
		for (int i = 0; i < parameter.length; i++) {
			parameterName[i] = "arg"+i;
			parameterType[i] = getParameterType(parameter[i].getName());  
			types.add(parameterType[i].toString());
		}
    
		// Generieren der Methode Calc
		mg = new MethodGen(Constants.ACC_PUBLIC, // access flags
				returnType, // return type
				parameterType, // arg types
				parameterName, // arg names
				myCalcMethodInfo.getName(), cg.getClassName(), // methodenname, klassenname
				il, cp); // instruction list & constant pool
    
		// ========================================================       
		// Aufruf der eigentlichen Code Generierungsmethode
		codegen.start();
		// ========================================================
		
		// fix null handles so it can be dumped
		InstructionHandle[] handles = il.getInstructionHandles();
		for (int i = 0; i < handles.length;i++) {
			Instruction ins = handles[i].getInstruction();
			if (ins instanceof BranchInstruction) {
				BranchInstruction br = (BranchInstruction)ins;
				if (br.getTarget() == null) {
					br.setTarget(handles[i]);
				}	
			}
		}

		mg.setMaxStack();
		mg.setMaxLocals(20);
		// Methode der Klasse hinzuf�gen
		cg.addMethod(mg.getMethod());
		// Constructor der Klasse hinzuf�gen
		cg.addEmptyConstructor(Constants.ACC_PUBLIC);
      
		dump();
		MyClassLoader ld = new MyClassLoader(cg.getJavaClass().getBytes());
		return ld.findClass("MyDynamicClass").newInstance();
	}
  
	// Instruktionlist ausgeben
	static void dump() {

		for (int i = 0; i < locals.size(); i++) {
			String type = (i < types.size())?types.get(i):"";
			System.out.println("LOCAL VAR[" + (i * 2 + 1) + "] " + "\"" + locals.get(i)+ "\" "+type);
		}
		Instruction ins[] = CodeGen.il.getInstructions();
		int addr = 0;
		for (int i = 0; i < ins.length; i++) {
			try {
				if (ins[i] instanceof BranchInstruction) {
					int jmpAddr = addr+((BranchInstruction)ins[i]).getIndex();
					System.out.println(
						addr + " " + ins[i].getName() + " " + 
									 ((jmpAddr != addr)?jmpAddr:"null"));
	
				}
				else {
						System.out.println(
						addr + " " + ins[i].toString(CodeGen.cp.getConstantPool()));
				}
			}
			catch (Exception e) {
				System.out.println(addr + " "+"ERROR");	
			}			
			addr += ins[i].getLength();
		}
	}
  
}
