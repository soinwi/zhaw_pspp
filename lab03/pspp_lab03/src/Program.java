import com.sun.org.apache.bcel.internal.generic.DRETURN;

public class Program implements ICodeGenStart {
	
	
	@Override
	public void start() throws Exception {
		// TODO Auto-generated method stub
		ProgramParser.program();
		
	}
	
	public static void main(String[] args) throws Exception
	{
		String code1 =  "m = arg0 - 42;"+
				"if (!m) {"+
				"return 7;"+
				"} else {"+
				"return 666;"+
				"}";
		
		String code2 = "m = arg0;"+
				"s = 1;"+
				"while (m) {"+
				"s = s * m;"+
				"m = m - 1;"+
				"}"+
				"return s;";
		
		Scanner.init(code2);
		//Scanner.init("if(!3){return 2;}return 0;");
		//Scanner.init("x=0; i=3; while(i){x = x+i; i = i-1;} return x;");
		CodeGen.init();
		Scanner.scan();
		IProgram prog = (IProgram)CodeGen.buildClass(IProgram.class, new Program());
		
		System.out.println("Result: " + prog.main(5));
				
	}

	
}
