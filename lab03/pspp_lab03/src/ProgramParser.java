import java.util.Stack;

import javax.swing.plaf.basic.DefaultMenuLayout;

import com.sun.org.apache.bcel.internal.*;
import com.sun.org.apache.bcel.internal.generic.*;

/**
 * User: Karl Rege
 */

//Wiederverwendet von lab02

class ProgramParser {
	
	static void program() throws Exception
	{
//		int arg0Slot = CodeGen.local("arg0");
//		CodeGen.il.append(new DSTORE(arg0Slot));
		
		statementSequence();
		
		//int retSlot = CodeGen.local("value");
		//CodeGen.il.append(new DLOAD(retSlot));
		//CodeGen.il.append(new DRETURN());
	}
	
	static void block() throws Exception
	{
		Scanner.check(Token.LCBRACK);
		
		statementSequence();
		Scanner.check(Token.RCBRACK);
		
	}
	
	static void returnStatement() throws Exception
	{
		if(Scanner.la == Token.RETURN)
		{
			Scanner.scan();
			
			expr();
			
			Scanner.check(Token.SCOLON);
			CodeGen.il.append(new DRETURN());
		}
	}
	
	static void ifStatement() throws Exception
	{
		Scanner.check(Token.IF);
		
		boolean inverted = condition();
		
		BranchHandle bh = null;
		//= CodeGen.il.append(new IFEQ(null));
		
		if(!inverted)
		{
			bh = CodeGen.il.append(new IFEQ(null));
		}
		else
		{
			bh = CodeGen.il.append(new IFNE(null));
		}
		
		statement();
		
		InstructionHandle nopHandle = CodeGen.il.append(new NOP());
		bh.setTarget(nopHandle);
		
		if(Scanner.la == Token.ELSE)
		{
			Scanner.scan();
			statement();
		}
		
		
	}
	
	static void whileStatement() throws Exception
	{
		Scanner.check(Token.WHILE);
		
		InstructionHandle beforeCond = CodeGen.il.append(new NOP());
		boolean inverted = condition();
		
		BranchHandle bh = null;
		
		if(!inverted)
		{
			bh = CodeGen.il.append(new IFEQ(null));
		}
		else
		{
			bh = CodeGen.il.append(new IFNE(null));
		}
		
		statement();
		CodeGen.il.append(new GOTO(beforeCond));
		
		
		InstructionHandle nopHandle = CodeGen.il.append(new NOP());
		bh.setTarget(nopHandle);
	}
	
	static boolean condition() throws Exception
	{
		Scanner.check(Token.LBRACK);
		
		boolean inverted = false;
		
		if(Scanner.la == Token.NOT)
		{
			Scanner.scan();
			inverted = true;
		}
		
		expr();
		
		CodeGen.il.append(new D2I());
		
		if(inverted)
		{
			CodeGen.il.append(new INEG());
		}
		
		Scanner.check(Token.RBRACK);
		
		return inverted;
		
	}
	
	static void statementSequence() throws Exception
	{
		statement();
		while(Scanner.la != Token.EOF && Scanner.la != Token.RCBRACK)
		{
			//Scanner.scan();
			statement();
		}
	}
	
	static void statement() throws Exception
	{
		if(Scanner.la == Token.RETURN)
		{
			returnStatement();
		}
		else if(Scanner.la == Token.IF)
		{
			ifStatement();
		}
		else if(Scanner.la == Token.WHILE)
		{
			whileStatement();
		}
		else if(Scanner.la == Token.LCBRACK)
		{
			block();
		}
		else
		{
			assignement();
		}
	}
	
	static void assignement() throws Exception
	{
		if(Scanner.la == Token.IDENT)
		{
			Scanner.scan();
			String ident = Scanner.token.str;
			int slot = CodeGen.local(ident);
			
			Scanner.check(Token.EQUAL);
			expr();
			
			Scanner.check(Token.SCOLON);
			CodeGen.il.append(new DSTORE(slot));
		}
		
	}

	static void expr()	throws Exception {
		term();
		while (Scanner.la == Token.PLUS 
				|| Scanner.la == Token.MINUS) {
			Scanner.scan();
			int op = Scanner.token.kind;
			term();
			if(op == Token.PLUS)
			{
				CodeGen.il.append(new DADD());
			}
			else
			{
				CodeGen.il.append(new DSUB());
			}
		}
	}

	static void term()	throws Exception {
		factor();
		while (Scanner.la == Token.TIMES || Scanner.la == Token.SLASH) {
			Scanner.scan();
			int op = Scanner.token.kind;
			factor();

			if(op == Token.TIMES)
			{
				CodeGen.il.append(new DMUL());
			}
			else
			{
				CodeGen.il.append(new DDIV());
			}
			
		}
	}

	static void factor() throws Exception {
		if (Scanner.la == Token.LBRACK) {
			Scanner.scan();
			expr();
			Scanner.check(Token.RBRACK);
		} else if (Scanner.la == Token.NUMBER) {
			Scanner.scan();
			int c = CodeGen.cp.addDouble(Scanner.token.val);
			CodeGen.il.append(new LDC2_W(c));
//			_stack.push(Scanner.token.val);
		}
		else if(Scanner.la == Token.IDENT){
			Scanner.scan();
			int slot = CodeGen.local(Scanner.token.str);
			CodeGen.il.append(new DLOAD(slot));
			
		}
	}


//   public static void main(String[] args) throws Exception {
//   	Scanner.init("x=3;value=2;");
//   	CodeGen.init();
//   	Scanner.scan();
//   	program();
//
//   }
      
}
