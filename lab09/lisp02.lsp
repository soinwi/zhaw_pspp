; aufgabe 1a
(defun reduce-list (f init lst)
	(if (null lst)
		init
		(funcall f (car lst) (reduce-list f init (cdr lst)))
	)
)

;aufgabe 1b

;diese funktion filter die liste, dass nur jene elemente zurückgegeben werden, welche der als f angegebenen bedingung entsprechen
(defun filter (f seq)
	(cond ((null seq) nil)
		((funcall f (car seq))
			(cons (car seq) (filter f (cdr seq))))
		(t (filter f (cdr seq)))))

; hilfsfunktionen aus letztem lab
(defun map-list (f lst)
	(if (null lst) nil
	;; else
	(cons (funcall f (car lst))
		(map-list f (cdr lst)))))
(defun all-true (l)
	(if (null l)
		T
		(if (car l)
			(all-true (cdr l))
			nil
		)
	)
)


; aufgabe 2
(defun list-n-and-square (lst)
	(map-list (lambda (n) (cons n (* n n)) ) lst)
)

(defun list-only-even (lst)
	(filter (lambda (n) (= 0 (mod n 2))) lst)
)

(defun list-open-tasks (tasks)
	(filter (lambda (tsk) (eq :open (cdr tsk) )) tasks)
)

;aufgabe 3
(defun range (from &optional to inc)
	(if (null to) (range 0 from inc)
		(if (= from to)
			'()
			(cons from (range (+ (if (null inc) 1 inc) from) to inc))
	    )
	)
)

;aufgabe 4
(defun fak (n)
	(reduce-list (lambda (a b)(* a b)) 1 (range 1 (+ 1 n)))
)