;; Read text file into string
;;
(defun file-to-string (path)
  (with-open-file (stream path)
    (let ((data (make-string (file-length stream))))
      (read-sequence data stream)
      data)))


;; Alternative implementation in case the Common Lisp used does not
;; support the read-sequence function
;;
(defun file-to-string (path)
  (with-open-file (stream path)
    (apply #'concatenate 
           (cons 'string 
                 (loop
                   for line = (read-line stream nil 'eof)
                   until (eq line 'eof)
                   collect (format nil "~A~%" line))))))


;; Split string at character
;;
(defun string-split (c strg &optional (start 0))
  (let ((end (position c strg :start start)))
    (cond (end (cons (subseq strg start end)
                     (string-split c strg (+ end 1))))
          (t (list (subseq strg start))))))


;; eigentlich ist die Bezeichnung autocurry für die folgende Funktion nicht ganz
;; korrekt, es ist eher ein autopartial
;;
(defun autocurry (f numargs)
  (lambda (&rest args)
    (cond ((>= (length args) numargs) 
            (apply f args))
          ((>= (length args) 1)
            (lambda (&rest restargs) (apply f (append args restargs))))
          (t (error "Function must be called with at least one argument")))))


;; Makro zum vereinfachten Binden einer Funktion an ein Symbol
;;
(defmacro setfun (symb fun)
  `(prog1 ',symb (setf (symbol-function ',symb) ,fun)))


;; Pipeline: Funktion erstellen, die aus der Verkettung einer Reihe von 
;; Funktionen gebildet wird 
;; 
(defun pipeline (&rest funcs)
  (lambda (&rest args)
    (car (reduce
          (lambda (ar f) (list (apply f ar)))
          funcs
          :initial-value args))))


;; Partielle Anwendung einer Funktion
;;
(defun partial (f &rest args)
  (lambda (&rest more-args)
    (apply f (append args more-args))))


(defun filter-fn (f seq)
  (remove-if-not f seq))

(defun reject-fn (f seq)
  (remove-if f seq))

(defun prop-eq (prop val)
  (pipeline (getprop prop) (partial #'equal val)))

;; antworten zu fragen:
; with-input-from-string öffnet einen stream auf eine string und verarbeitet dieses mit einer anzugebenen funtkion
; unterschiede: sehr ähnlich, z.b. dueDate nach due-date (pascalCase nach bindestrich-getrennt)

(defun read-json ()
  (read-from-file "lab12\\fp_praktikum_2\\tasklist\\tasks.lisp"))


(defun getprop-fn (sym lst)
  (cond ((null lst) NIL)
        ((eq (caar lst) sym) (cdar lst))
        (t (getprop-fn sym (cdr lst)))
      )
)

(defvar *tasks* (getprop :tasks (read-json)))

(setfun getprop (autocurry #'getprop-fn 2) )

; filter-fn: gibt nur jene elemente zurück, die einer bedingung entsprechen
; reject-fn: entfernt jene/gibt nicht zurück, die einer bedingung entsprechen
; prop-eq: gibt eine funktion zurück, die auf eine liste angewendet werden kann. somit kann das ergebnis dieser funktion als bedingung für obige beide verwendet werden.


(setfun filter (autocurry #'filter-fn 2))

(setfun reject (autocurry #'reject-fn 2))

(defun pick-fn (attrs obj)
  (remove-if-not #'(lambda (el) (member (car el) attrs)) obj))
(setfun pick (autocurry #'pick-fn 2))
(setfun forall (autocurry #'mapcar 2))

; pick wählt nur einige der attribute (listen-einträge der einzelnen tasks) aus

(forall (pick '(:TITLE :DUE-DATE)) *tasks*)

(defun date-to-universal (datestr)
  
  (encode-universal-time 0 0 0 (parse-integer (cadr (string-split #\/ datestr))) (parse-integer (car (string-split #\/ datestr))) (parse-integer (caddr (string-split #\/ datestr))))  
)

(defun sort-by-fn (f seq)
  (sort (copy-list seq)
    (lambda (a b) (< (funcall f a) (funcall f b)))))

(setfun sort-by (autocurry #'sort-by-fn 2))

(defun open-tasks (name)
  (pipeline
    (getprop :tasks)
    (filter (prop-eq :member name))
    (reject (prop-eq :complete t))
    (forall (pick '(:id :due-date :title :priority)))
    (sort-by (pipeline (getprop :due-date) #'date-to-universal))))

; aufruf mit daten:
(funcall (open-tasks "Scott") (read-json))