(+ 3 8)

(defun flaeche(x  y)
	(* x y)
)

(defun sqrlist (s)
	(if (null s)
		'()
		(cons (expt (car s) 2)
		(sqrlist (cdr s)))))

;aufgabe 1a (rest nur "ausprobiert")
(- (* 10 (+ 4 3 3 1)) 20)

;aufgabe 2a
(defun list-double (l)
	(if (null l)
		'()
		(cons (* 2 (car l)) (list-double (cdr l)))
	)
)

(defun sign (z)
	(if (> z 0)
		1
		-1
	)
)

(defun list-sign (l)
	(if (null l)
		'()
		(cons (sign (car l)) (list-sign (cdr l)))
	)
)

;aufgabe 3
(defun map-list (f lst)
	(if (null lst) nil
	;; else
	(cons (funcall f (car lst))
		(map-list f (cdr lst)))))

(defun list-sign-new (l)
	(map-list (function sign) l)
)

(defun list-double-new (l)
	(map-list (lambda (n) (* 2 n)) l)
)

;aufgabe 4
(defun list-sum (l)
	(if (null l)
		0
		(+ (car l) (list-sum (cdr l)))
	)
)

(defun list-mul (l)
	(if (null l)
		1
		(* (car l) (list-mul (cdr l)))
	)
)
; unterschied zwischen den beiden: anderes neutralelement (1 bzw 0)

(defun all-true (l)
	(if (null l)
		T
		(if (car l)
			(all-true (cdr l))
			nil
		)
	)
)