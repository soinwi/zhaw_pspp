	program pival
	implicit none

	integer :: numtrue, count, i
	real :: x,y,pi, tempc

	numtrue = 0
	pi = 0

	print*, "enter count"
	read*, count

	do i = 0,count,1

		x = rand(0)
		y = rand(0)

		!print*, x
		!print*, y

		tempc = (sqrt(x**2 + y**2))
		!print*, "tempc: ", tempc
		if(tempc .lt. 1) then
			numtrue = numtrue + 1
			!print*, "inside pi, sum: ", numtrue
		end if

	end do

	pi = (real(numtrue) / real(count)) * real(4)

	print*, "pi: ", pi

	end
