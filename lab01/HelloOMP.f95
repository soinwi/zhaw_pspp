
function ran0(seed)
! Park & Miller Random Generator
integer seed,ia,im,iq,ir,mask,k
real ran0,am
parameter (ia=16807,im=2147483647,am=1./im, iq=127773,ir=2836,mask=123459876)
seed=ieor(seed,mask)
k=seed/iq
seed=ia*(seed-k*iq)-ir*k
if (seed.lt.0) seed=seed+im
ran0=am*seed
seed=ieor(seed,mask)
return
end

program helloomp
 use omp_lib
 implicit none

 real :: ran0
 real :: sum, sum_local, sum_global, pi, x, y, tempc
 integer*4 tid,i, num
 integer*4, parameter :: numthreads = 4

 num = 100000000
 sum_global = 0
!$omp parallel private(sum_local,tid,i) num_threads(numthreads)
 tid = omp_get_thread_num()
 print '("hello from thread = ",i2)', tid
 sum_local = 0
 do i = 1, num/numthreads
 		x = ran0(tid)
		y = ran0(tid)

		!print*, x
		!print*, y

		tempc = (sqrt(x**2 + y**2))
		!print*, "tempc: ", tempc
		if(tempc .lt. 1) then
			sum_local = sum_local + 1
			!print*, "inside pi, sum: ", numtrue
		end if
    
 end do
!$omp critical
 sum_global = sum_global + sum_local
 !$omp end critical
!$omp end parallel
print '("sum_global = ",f10.0)',sum_global

pi = (real(sum_global) / real(num)) * real(4)
print*, "pi: ", pi


end


