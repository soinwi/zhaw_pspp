MODULE Buffon;

FROM RealRandomNumbers IMPORT Randomize,Random;
FROM Terminal IMPORT WriteString,WriteLn,ReadChar;
FROM SRealIO  IMPORT WriteReal;
FROM SWholeIO IMPORT WriteCard;
FROM StdChans IMPORT SetOutChan;
FROM TermFile IMPORT ChanId, Open, read, write, echo, OpenResults;
FROM RealMath IMPORT cos;
IMPORT RealMath;

VAR r: REAL;
 ch : CHAR;
 term: ChanId;
 resOpen: OpenResults;

 pos, sin, pi:LONGREAL;

 i, numhit, counter : CARDINAL;

BEGIN
    Open (term, write+read+echo, resOpen);
    SetOutChan(term);

    Randomize(0);

    pi := 0.0;

    FOR i := 0 TO 1000000 BY 1 DO
        pos := 1.0 - Random();                  (*position des ersten punkts der geraden, von oberer linie gesehen*)
        sin := cos(Random()/2.0*RealMath.pi);   (*sinus des winkels, somit "h�he" der linie*)

        counter := counter +1;

        IF pos+sin > 1.0 THEN                   (* wenn y-position von P1 + "h�he" der linie > 1 ist, wird die linie ber�hrt/geschnitten*)
            numhit := numhit +1;
        END;

        IF counter MOD 100 = 0 THEN (*performanter, wenn ausgabe nur alle 100 schritte*)

        pi := 2.0*FLOAT(counter)/FLOAT(numhit);
        WriteString("count: ");
        WriteCard(counter,10);
        WriteString(" hits: ");
        WriteCard(numhit,10);
        WriteLn();

        WriteString("pi: ");
        WriteReal(pi,10);
        WriteLn();

        END;


    END;

    ch := ReadChar();


END Buffon.
