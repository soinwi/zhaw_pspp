(load "lab11\\pspp_basics.lisp")

; fragen:
; das zwischenresultat wird von schritt zu schritt migegeben

(defun factorial (n &optional (fact 1))
	(if (<= n 1) fact
		(partial #'factorial (- n 1) (* n fact))))

; aufruf:
;(apply (apply (factorial 3) NIL) NIL)
; (oder einfacher mit trampoline)
; (trampoline (factorial 4))

(defun trampoline (fun &rest args)
	(let ((result (apply fun args)))
		(loop while (functionp result) do
			(setq result (funcall result)))
		result))