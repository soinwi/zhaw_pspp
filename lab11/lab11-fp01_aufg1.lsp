(load "lab11\\pspp_basics.lisp")

(defun range (from &optional to inc)
	(if (null to) (range 0 from inc)
		(if (= from to)
			'()
			(cons from (range (+ (if (null inc) 1 inc) from) to inc))
	    )
	)
)

(defun repeatedly (times fun)
  (mapcar fun (range times)))


; aufgabe 1
; mehrfach hello
(repeatedly 5 (lambda (n) 'hello))
;wuerfelergebnisse
(repeatedly 5 (lambda (n) (+ 1 (random 6))))
;id0-id4
(repeatedly 5 (lambda (n) (concatenate 'string "id" (write-to-string n))))

; implementation of always
(defun always (value)
  (lambda (&optional &rest n) value))
;antworten zu fragen:
; ja, es wird closure verwendet (rückgabewert wird innerhalb der zurückgegebenen funktion gespeichert)
(REPEATEDLY 5 (always 'HELLO))
; ja, denn sie liefert eine funktion zurück



