(load "lab11\\pspp_basics.lisp")


(setfun num (dispatch #'parse-int #'parse-float #'identity))

(defun num-args (&rest args)
	(mapcar #'num args))

(setfun add (wrap-fn #'+ #'num-args))

; antworten zu fragen:
; num wandelt eine eingabe in eine zahl um, egal ob es sichum integer- oder float-strings handelt ode rum eine zahl
; beispiele: (num "12") (num "12.5") (num 13.8)
; identity gibt die identität, das heisst den wert selbst zurück

; add: addiert alle angegebenene werte zusammen. parst int oder float, wenn als string übergeben.
; wrap-fn wird verwendet, um als preprocessing die strings in zahlen umzuwandeln. bsp: (add 1 "2" "3.7")

; sinnvolle verbesserung: bei (einzelnen) ungültigen werten z.B. "a" wird abgebrochen. evtl anders besser?

