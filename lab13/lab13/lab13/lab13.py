from adder import adder

print("hello")

def strcodes(str):
    for s in str:
        print(ord(s), end=' ')
strcodes("Hallo")
print("\n")

print(list(map(lambda c: ord(c), "Hallo")))

def printdict(dict):
    print( list(map(lambda e: e + ": " + str(dict[e]), sorted(dict))) )
printdict({"beta": 22, "alfa": 11, "gamma": 33})

def addDict(dict1, dict2):
    dictRet = dict2.copy()
    for k,v in dict1.items():
        dictRet[k] = v
    return dictRet

addDict({1:111, 2:222}, {2:999, 3:333})

# Zeigt zur verf�gung stehende methoden/felder an. sind unterschiedlich f�r liste und dicts.

def union(list1, list2, returnSorted=True):
    retlist = list1.copy()
    for e in list2:
        if(e not in retlist):
            retlist.append(e)
    if(returnSorted):
        return sorted(retlist)
    else:
        return retlist

union([1, 2, 3], [3, 4]) 

def diff(list1, list2):
    retlist = list1.copy()
    for e in list2:
        if(e in retlist):
            retlist.remove(e)
    return sorted(retlist)
diff([1, 2, 3], [3, 4])

def flatten(unflat):
    retlist = []
    for e in unflat:
        if(isinstance(e, list) or isinstance(e, tuple)):
            retlist = union(retlist, flatten(e), False)
        elif e not in retlist:
            retlist.append(e)
    return retlist
flatten([[[1, 2], 3], [[4]], (5, 6), ([7])])

a = adder()
print("adder: " + str(a.add(12,19)))


# was macht testthis?
# testhis ruft eine website ab, entfernt alle tags aus dem html-code und gibt danach alle w�rter (getrennt durch leerzeichen) als liste zur�ck