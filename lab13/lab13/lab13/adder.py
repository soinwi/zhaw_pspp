
class adder(object):
    """description of class"""

    def add(self, num1, num2):
        return num1+num2


version = 1.0
if __name__ == '__main__':
    print('Module testcode started separately...')
    a = adder()
    print("3 + 4 should be 7, actually is: " + str( a.add(3,4)))
    print("8 + (-3) should be 5, actually is: " + str(a.add(8, -3)))