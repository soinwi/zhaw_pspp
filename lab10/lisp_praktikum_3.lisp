;; file-to-string
;; Read text file into string
;;
(defun file-to-string (path)
  (with-open-file (stream path)
    (let ((data (make-string (file-length stream))))
      (read-sequence data stream)
      data)))


;; string-split
;; Split string at character
;;
(defun string-split (c strg &optional (start 0))
  (let ((end (position c strg :start start)))
    (cond (end (cons (subseq strg start end)
                     (string-split c strg (+ 1 end))
                )
          )
          (t (list (subseq strg start))))
  )
)


;; parse-float
;; Parse string to float if possible, else return NIL
;;
(defun parse-float (strg)
  (with-input-from-string (s strg)
    (let ((res (read s)))
      (if (eq (type-of res) 'single-float) res nil))))


;; zip-to-alist
;; Merge two lists to an alist
;;
(defun zip-to-alist (lst1 lst2)
  (cond ((or (null lst1) (null lst2)) nil)
        (t (cons (cons (car lst1) (car lst2)) 
                 (zip-to-alist (cdr lst1) (cdr lst2))))))


(defun simple-csv (csvstring)
    (csv-from-lines (get-csv-linestrings csvstring))
)

(defun csv-from-lines (csvlines)
  (mapcar (lambda (bodyline) (zip-to-alist (get-header csvlines) (split-line bodyline))) 
          (get-body-strings csvlines)
  )
)

(defun get-header (csvlines)
  (get-csv-headers (car csvlines)))

(defun get-body-strings (csvlines)
  (cdr csvlines) )

(defun split-line (stringline)
  (string-split #\, stringline))

(defun linetolist (csvlinestring headers)
  (reverse (pairlis 
             (get-csv-headers (list csvlinestring))
             (string-split #\, csvlinestring)))
  )

(defun get-csv-linestrings (csvstring)
  (string-split #\Newline csvstring)
)

(defun get-csv-headers (headerline)
  (string-split #\, headerline))