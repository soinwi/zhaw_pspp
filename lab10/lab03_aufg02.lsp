; from lisp lab 02
(defun filter (f seq)
	(cond ((null seq) nil)
		((funcall f (car seq))
			(cons (car seq) (filter f (cdr seq))))
		(t (filter f (cdr seq)))))



(defun select-col (colname seq)
  (mapcar (lambda (line) 
            (cdar (filter (lambda (col) (string= (car col) colname)) line))
          ) seq ))

(defun reduce-list (f init lst)
	(if (null lst)
		init
		(funcall f (car lst) (reduce-list f init (cdr lst)))
	)
)

; Aufgabe 2
; precondition: (simple-csv-result is saved to *products*)

(load "lab10\\lisp_praktikum_3.lisp")
(defvar *products* (simple-csv (file-to-string "lab10\\products.csv")))


; a)
(select-col "Kategorie" *products*)

; b)
(defun unique (seq &key test)
  (cond ((null seq) NIL)
        ((filter (lambda (n) (funcall test n (car seq) )) (cdr seq)) (unique (cdr seq) :test test))
        (t (cons (car seq) (unique (cdr seq) :test test)))
  )
)

(unique (select-col "Kategorie" *products*) :test #'string=)

; c)
(defun prod-categories (products)
  (unique (select-col "Kategorie" products) :test #'string=)
  )

; d)
(defun prod-avg-price (products)
  (/ (reduce-list #'+ 0 (mapcar #'parse-float (select-col "Preis" products)) ) (length products))
  )