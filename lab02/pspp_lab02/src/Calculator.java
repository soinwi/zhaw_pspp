import java.util.Stack;

/**
 * User: Karl Rege
 */


class Calculator {
	
	private static Stack<Double> _stack = new Stack<Double>();

	static void expr()	throws Exception {
		term();
		while (Scanner.la == Token.PLUS 
				|| Scanner.la == Token.MINUS) {
			Scanner.scan();
			int op = Scanner.token.kind;
			term();
			double second = _stack.pop();
			double first = _stack.pop();
			double result = 0;
			if(op == Token.PLUS){ result = first + second;}else{result = first - second;}
			_stack.push(result);
		}
	}

	static void term()	throws Exception {
		factor();
		while (Scanner.la == Token.TIMES || Scanner.la == Token.SLASH) {
			Scanner.scan();
			int op = Scanner.token.kind;
			factor();
			double second = _stack.pop();
			double first = _stack.pop();
			double result = 0;
			if(op == Token.TIMES){result = first * second;}else{result = first / second;}
			_stack.push(result);
		}
	}

	static void factor() throws Exception {
		if (Scanner.la == Token.LBRACK) {
			Scanner.scan();
			expr();
			Scanner.check(Token.RBRACK);
		} else if (Scanner.la == Token.NUMBER) {
			Scanner.scan();
			_stack.push(Scanner.token.val);
		}
		else if(Scanner.la == Token.IDENT){
			Scanner.scan();
			_stack.push(Scanner.token.val);
		}
	}


   public static void main(String[] args) throws Exception {
   	Scanner.init("4*PI+E");
   	Scanner.scan();
   	expr();
   	System.out.println("result="+_stack.pop());
   }
      
}
